﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace DelegatesAndEvents {

    public class ObservableList<TItem> : IObservableList<TItem>
    {
        List<TItem> list = new List<TItem>();
        public IEnumerator<TItem> GetEnumerator()
        {
            return list.GetEnumerator();
            //throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(TItem item)
        {
            if (item != null)
                list.Add(item);
            else
                throw new ArgumentNullException();
            //throw new NotImplementedException();
        }

        public void Clear()
        {
            foreach (TItem item in list) {
                list.Remove(item);
            }
            //throw new NotImplementedException();
        }

        public bool Contains(TItem item)
        {
            foreach (TItem elem in list) {
                if (elem.Equals(item))
                    return true;
            }
            return false;
            //throw new NotImplementedException();
        }

        public void CopyTo(TItem[] array, int arrayIndex)
        {
            list.CopyTo(array, arrayIndex);
            //throw new NotImplementedException();
        }

        public bool Remove(TItem item)
        {
            foreach (TItem elem in list)
            {
                if (elem.Equals(item)) {
                    list.Remove(item);
                    return true;
                }
                   
            }
            return false;
            //throw new NotImplementedException();
        }

        public int Count
        {
            get
            {
                return list.Count;
                //throw new NotImplementedException();
            }
        }

        public bool IsReadOnly
        {
            get
            {
                list.AsReadOnly();
                return true;
                //throw new NotImplementedException();
            }
        }
        public int IndexOf(TItem item)
        {
            TItem[] array = list.ToArray();
            for (int i = 0; i < list.Capacity; i++) {
                if (array[i].Equals(item))
                    return i;
            }
            return -1;
            //throw new NotImplementedException();
        }

        public void Insert(int index, TItem item)
        {
            TItem[] array = list.ToArray();
            array[index] = item;
            
            //array.OfType<TItem>().ToList();
            //list.Insert(index, item);
            //throw new NotImplementedException();
        }

        public void RemoveAt(int index)
        {
            throw new NotImplementedException();
        }

        public TItem this[int index]
        {
            get { return list[index]; }
            set { list[index] = value; }
        }

        public event ListChangeCallback<TItem> ElementInserted;
        public event ListChangeCallback<TItem> ElementRemoved;
        public event ListElementChangeCallback<TItem> ElementChanged;

        public override string ToString()
        {
            return base.ToString();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

}