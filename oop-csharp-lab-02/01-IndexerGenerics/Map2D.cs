﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IndexerGenerics
{
    public class Map2D<TKey1, TKey2, TValue> : IMap2D<TKey1, TKey2, TValue>
    {
        /*
         * Suggerimento: come struttura dati interna per la memorizzazione dei valori della mappa
         * si suggerisce di utilizzare un Dictionary generico contenente come chiave una tupla di due
         * valori (le chiavi della mappa) e come valore il valore della mappa.
         * 
         * Esempio:
         * private IDictionary<Tuple<TKey1,TKey2>,TValue> values;
         */
        private IDictionary<Tuple<TKey1, TKey2>, TValue> dic;
        public Map2D()
        {
            this.dic = new Dictionary<Tuple<TKey1, TKey2>, TValue>();   
        }

        public void Fill(IEnumerable<TKey1> keys1, IEnumerable<TKey2> keys2, Func<TKey1, TKey2, TValue> generator)
        {
            /*
             * Nota: si verifichi il funzionamento del metodo Invoke() su oggetti di classe Func
             */
            foreach (TKey1 tkey in keys1) {
                foreach (TKey2 tkey2 in keys2) {
                    dic.Add(Tuple.Create<TKey1, TKey2>(tkey, tkey2), generator.Invoke(tkey,tkey2));
                }
            }

            //throw new NotImplementedException();
        }

        public bool Equals(IMap2D<TKey1, TKey2, TValue> other)
        {
            if (dic.Count == other.NumberOfElements)
            {
                foreach (Tuple<TKey1, TKey2, TValue> tuple in other.GetElements())
                {
                    if (!(tuple.Item3.Equals(dic[new Tuple<TKey1,TKey2>(tuple.Item1, tuple.Item2)]))) {
                        return false;
                    }
                }
                return true;
            }
            return false;
            
            //throw new NotImplementedException();
        }

        public TValue this[TKey1 key1, TKey2 key2]
        {
            /*
             * Suggerimento: si noti che sulla classe Dictionary è definito un indicizzatore per i valori di chiave
             */

            get
            {
                return dic[new Tuple<TKey1, TKey2>(key1,key2)]; 
                //throw new NotImplementedException();
            }

            set
            {
                dic.Add(Tuple.Create<TKey1, TKey2>(key1, key2), value);
                //throw new NotImplementedException(); 
            }
        }

        public IList<Tuple<TKey2, TValue>> GetRow(TKey1 key1)
        {
            IList<Tuple<TKey2, TValue>> list = new List<Tuple<TKey2, TValue>>();
            foreach (Tuple<TKey1, TKey2> tuple in dic.Keys) {
                if(key1.Equals(tuple.Item1))
                    list.Add(Tuple.Create<TKey2,TValue>(tuple.Item2, dic[tuple]));
            }
            return list;
            //throw new NotImplementedException();
        }

        public IList<Tuple<TKey1, TValue>> GetColumn(TKey2 key2)
        {
            IList<Tuple<TKey1, TValue>> list = new List<Tuple<TKey1, TValue>>();
            foreach (Tuple<TKey1, TKey2> tuple in dic.Keys)
            {
                if (key2.Equals(tuple.Item2))
                    list.Add(Tuple.Create<TKey1, TValue>(tuple.Item1, dic[new Tuple<TKey1, TKey2>(tuple.Item1, tuple.Item2)]));
            }
            return list;
            //throw new NotImplementedException();
        }

        public IList<Tuple<TKey1, TKey2, TValue>> GetElements()
        {
            IList<Tuple<TKey1,TKey2, TValue>> list = new List<Tuple<TKey1,TKey2, TValue>>();
            foreach (Tuple<TKey1, TKey2> tuple in dic.Keys)
            {
                list.Add(Tuple.Create<TKey1,TKey2, TValue>(tuple.Item1, tuple.Item2, dic[new Tuple<TKey1, TKey2>(tuple.Item1, tuple.Item2)]));
            }
            return list;
            //throw new NotImplementedException();
        }

        public int NumberOfElements
        {
            get
            {
                return this.dic.Count;
                //throw new NotImplementedException();
            }
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
